
package Modelo;

/**
 *
 * @author quier
 */
public class Cotizacion {
    private int codigo;
    private String descripcion;
    private float precio;
    private int porcentajePagoInicial;
    private int plazoPago;
    
    public Cotizacion(int codigo, String descripcion, float precio, int porcentajePagoInicial, int plazoPago) {
        this.codigo = codigo;
        this.descripcion = descripcion;
        this.precio = precio;
        this.porcentajePagoInicial = porcentajePagoInicial;
        this.plazoPago = plazoPago;
    }
    
    public Cotizacion(Cotizacion otro) {
        this.codigo = otro.codigo;
        this.descripcion = otro.descripcion;
        this.precio = otro.precio;
        this.porcentajePagoInicial = otro.porcentajePagoInicial;
        this.plazoPago = otro.plazoPago;
    }
    
    public Cotizacion() {
        this.codigo = 0;
        this.descripcion = "";
        this.precio = 0;
        this.porcentajePagoInicial = 0;
        this.plazoPago = 0;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public float getPrecio() {
        return precio;
    }

    public void setPrecio(float precio) {
        this.precio = precio;
    }

    public int getPorcentajePagoInicial() {
        return porcentajePagoInicial;
    }

    public void setPorcentajePagoInicial(int porcentajePagoInicial) {
        this.porcentajePagoInicial = porcentajePagoInicial;
    }

    public int getPlazoPago() {
        return plazoPago;
    }

    public void setPlazoPago(int plazoPago) {
        this.plazoPago = plazoPago;
    }
    
    public float calcularPagoInicial() {
        return getPrecio() * (getPorcentajePagoInicial() / 100);
    }
    
    public float calcularTotalFinanciar() {
        return getPrecio() - calcularPagoInicial();
    }
    
    public float calcularPagoMensual() {
        return calcularTotalFinanciar() / getPlazoPago();
    }
    
}
